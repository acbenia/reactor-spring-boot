CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
	nickname VARCHAR ( 50 ) unique
);

CREATE TABLE mail (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
	sender VARCHAR ( 50 ),
	receiver VARCHAR ( 50 ),
	subject VARCHAR,
	body VARCHAR,
	sendTime TIMESTAMPTZ default CURRENT_TIMESTAMP,
	CONSTRAINT fk_sender
      FOREIGN KEY(sender)
	  REFERENCES users(nickname),
	CONSTRAINT fk_receiver
      FOREIGN KEY(receiver)
	  REFERENCES users(nickname)
);