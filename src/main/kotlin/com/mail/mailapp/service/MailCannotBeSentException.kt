package com.mail.mailapp.service

class MailCannotBeSentException(val reason: String) : RuntimeException()