package com.mail.mailapp.service

import com.mail.mailapp.model.Mail
import com.mail.mailapp.model.User
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


interface MailService {

    fun createUser(nickname: String?): Mono<User>
    fun createUser(user: User): Mono<User>
    fun sendMail(mail: Mail): Mono<Mail>
    fun getMailsInbox(nickname: String): Flux<Mail>
    fun getSentMails(nickname: String): Flux<Mail>
}
