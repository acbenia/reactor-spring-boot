package com.mail.mailapp.service

class UserAlreadyExistsException(val nickName: String?) : RuntimeException()