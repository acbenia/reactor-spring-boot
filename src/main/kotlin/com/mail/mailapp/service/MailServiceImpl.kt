package com.mail.mailapp.service

import com.mail.mailapp.model.Mail
import com.mail.mailapp.model.User
import com.mail.mailapp.repo.MailRepository
import com.mail.mailapp.repo.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Service
class MailServiceImpl : MailService {

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var mailRepository: MailRepository

    @Transactional
    override fun createUser(nickname: String?): Mono<User> {
        return userRepository
                .findByNickname(nickname)
                .flatMap {
                    Mono.error<User>(UserAlreadyExistsException(nickname))
                }
                .switchIfEmpty(userRepository.createByNickname(nickname))
    }

    override fun createUser(user: User): Mono<User> {
        return createUser(user.nickname)
    }



    @Transactional
    override fun sendMail(mail: Mail): Mono<Mail> {
        if (mail.sender == mail.receiver) {
            return Mono.error<Mail>(MailCannotBeSentException("sender should be different from the receiver!"))
        }
        return Mono.zip(
                userRepository.findByNickname(mail.sender),
                userRepository.findByNickname(mail.receiver)
        ).flatMap {
            mailRepository.save(mail)
        }
        .switchIfEmpty(Mono.error(MailCannotBeSentException("One of the users cannot be found")))
    }

    override fun getMailsInbox(nickname: String): Flux<Mail> = mailRepository.getMailsInbox(nickname)

    override fun getSentMails(nickname: String): Flux<Mail> = mailRepository.getSentMails(nickname)

}

