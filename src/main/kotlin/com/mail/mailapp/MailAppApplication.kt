package com.mail.mailapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MailAppApplication

fun main(args: Array<String>) {
	runApplication<MailAppApplication>(*args)
}
