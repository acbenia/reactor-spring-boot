package com.mail.mailapp.model

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonIgnore
import com.mail.mailapp.service.MailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.util.*

@Table("users")
open class User : Persistable<UUID> {

    @Column("nickname")
    var nickname: String? = null

    @Id
    @Column("id")
    @JsonIgnore
    var iden: UUID? = null

    @Transient
    @JsonIgnore
    override fun getId(): UUID? {
        return iden
    }

    @Transient
    @JsonIgnore
    override fun isNew(): Boolean {
        return iden == null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as User
        if (nickname != other.nickname) return false
        return true
    }

    override fun hashCode(): Int {
        return nickname?.hashCode() ?: 0
    }

}