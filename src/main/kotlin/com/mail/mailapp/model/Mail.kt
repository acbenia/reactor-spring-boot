package com.mail.mailapp.model

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.Temporal
import javax.persistence.TemporalType

@Table("mail")
@Entity
open class Mail : Persistable<UUID> {
    @Column("id")
    @Id
    var iden: UUID? = null

    @Column("sender")
    var sender: String? = null

    @Column("receiver")
    var receiver: String? = null

    @Column("subject")
    var subject: String? = null

    @Column("body")
    var body: String? = null

    @Column("sendTime")
    @Temporal(TemporalType.TIMESTAMP)
    var sendTime: LocalDateTime? = null

    @Transient
    override fun getId(): UUID? {
        return iden
    }

    @Transient
    override fun isNew(): Boolean {
        return iden == null
    }
}