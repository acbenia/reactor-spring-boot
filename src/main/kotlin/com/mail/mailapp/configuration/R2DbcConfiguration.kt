package com.mail.mailapp.configuration

import io.r2dbc.spi.ConnectionFactory
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration

@Configuration
internal class R2DbcConfiguration(private val connectionFactory: ConnectionFactory) : AbstractR2dbcConfiguration() {
    override fun connectionFactory(): ConnectionFactory {
        return connectionFactory
    }

}