package com.mail.mailapp.configuration

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration
import io.r2dbc.postgresql.PostgresqlConnectionFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spring.datasource")
internal class PostgresConnectionConfig(
        @Value("\${spring.datasource.port}") var port: Int,
        @Value("\${spring.datasource.host}") var host: String,
        @Value("\${spring.datasource.database}") var database: String,
        @Value("\${spring.datasource.username}") var username: String,
        @Value("\${spring.datasource.password}") var password: String) {


    @Bean
    fun connectionFactory(): PostgresqlConnectionFactory {
        val config = PostgresqlConnectionConfiguration.builder()
                .host(host)
                .port(port)
                .database(database)
                .username(username)
                .password(password)
                .build()
        return PostgresqlConnectionFactory(config)
    }
}