package com.mail.mailapp.controller

import com.mail.mailapp.model.Mail
import com.mail.mailapp.model.User
import com.mail.mailapp.service.MailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
class MailController {

    @Autowired
    lateinit var mailService: MailService

    @PostMapping("/users")
    fun createUser(@RequestBody user: User): Mono<*> {
        return mailService.createUser(user.nickname)
    }

    @PostMapping("/mails/{user}")
    fun sendMail(@PathVariable("user") user: String, @RequestBody mailRequest: MailRequest): Mono<*> {
        val mail: Mail = Mail()
        mail.sender = user
        mail.receiver = mailRequest.to
        mail.body = mailRequest.body
        mail.subject = mailRequest.subject
        return mailService.sendMail(mail)
    }

    @GetMapping("/mails/{user}/inbox")
    fun getAllReceivedMails(@PathVariable("user") user: String): Flux<Mail>{
        return mailService.getMailsInbox(user)
    }

    @GetMapping("/mails/{user}/sent")
    fun getAllSentEmails(@PathVariable("user") user: String): Flux<Mail>{
        return mailService.getSentMails(user)
    }
}
