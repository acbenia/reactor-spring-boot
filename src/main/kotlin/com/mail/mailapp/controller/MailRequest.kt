package com.mail.mailapp.controller

open class MailRequest{
    var to: String? = null
    var subject: String? = null
    val body: String? = null
}