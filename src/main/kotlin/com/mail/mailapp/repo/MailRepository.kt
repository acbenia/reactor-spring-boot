package com.mail.mailapp.repo

import com.mail.mailapp.model.Mail
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux
import java.util.*

interface MailRepository : ReactiveCrudRepository<Mail, UUID> {

    @Query("SELECT * from mail where receiver = :nickname order by sendtime asc ")
    fun getMailsInbox(nickname: String): Flux<Mail>

    @Query("SELECT * from mail where sender = :nickname order by sendtime asc ")
    fun getSentMails(nickname: String): Flux<Mail>
}