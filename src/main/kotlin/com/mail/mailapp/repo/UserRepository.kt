package com.mail.mailapp.repo

import com.mail.mailapp.model.User
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Mono
import java.util.*

interface UserRepository : ReactiveCrudRepository<User, String> {

    fun findByNickname(nickname: String?): Mono<User>

    @Query("insert into users(nickname) values (:nickname) returning *")
    fun createByNickname(nickname: String?): Mono<User>
}