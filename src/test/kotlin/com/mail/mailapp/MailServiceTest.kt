package com.mail.mailapp


import com.mail.mailapp.model.Mail
import com.mail.mailapp.model.User
import com.mail.mailapp.repo.UserRepository
import com.mail.mailapp.service.MailService
import com.mail.mailapp.service.UserAlreadyExistsException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Mono
import reactor.test.StepVerifier


@SpringBootTest
class MailServiceTest : AbstractIntegrationTest() {

    @Autowired
    lateinit var mailService: MailService

    @Autowired
    lateinit var userRepository: UserRepository

    @Test
    fun saveUserTest() {
        val userMono: Mono<User> = mailService.createUser("user1")
        StepVerifier
                .create(userMono)
                .assertNext { user: User ->
                    Assertions.assertEquals("user1", user.nickname)
                }
                .expectComplete()
                .verify()
    }

    @Test
    fun recreateTheSameUser() {
         mailService.createUser("user2").block()
        val userMono: Mono<User> = mailService.createUser("user2")
        StepVerifier
                .create(userMono)
                .expectError(UserAlreadyExistsException::class.java)
                .verify()
    }

    @Test
    fun sendMail() {
        userRepository.createByNickname("user3").block()
        userRepository.createByNickname("user4").block()

        val mail = Mail()
        mail.sender = "user3"
        mail.receiver = "user4"
        mail.subject = "test subject"
        mail.body = "test body"
        val userMono: Mono<Mail> = mailService.sendMail(mail)
        StepVerifier
                .create(userMono)
                .assertNext {
                    Assertions.assertEquals("test body", mail.body)
                }
                .expectComplete()
                .verify()
    }
}
