package com.mail.mailapp

import org.flywaydb.core.Flyway
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.core.env.MapPropertySource
import org.springframework.test.context.ContextConfiguration
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.lifecycle.Startables
import java.util.stream.Stream

@ContextConfiguration(initializers = [AbstractIntegrationTest.Initializer::class])
open class AbstractIntegrationTest {
    internal class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
        override fun initialize(
                applicationContext: ConfigurableApplicationContext) {
            startContainers()
            val environment = applicationContext.environment
            val testcontainers = MapPropertySource(
                    "testcontainers",
                    createConnectionConfiguration()
            )
            initDBSchema()
            environment.propertySources.addFirst(testcontainers)
        }

        companion object {
            var postgres = PostgreSQLContainer<Nothing>()

            private fun startContainers() {
                Startables.deepStart(Stream.of(postgres)).join()
            }

            private fun createConnectionConfiguration(): Map<String, String> {
                return java.util.Map.of(
                        "spring.datasource.host", postgres.host,
                        "spring.datasource.port", "${postgres.firstMappedPort}",
                        "spring.datasource.database", postgres.databaseName,
                        "spring.datasource.username", postgres.username,
                        "spring.datasource.password", postgres.password
                )
            }

            private fun initDBSchema() {
                val flyway = Flyway
                        .configure()
                        .dataSource(
                                postgres.jdbcUrl,
                                postgres.username,
                                postgres.password
                        )
                        .load()
                flyway.clean()
                flyway.migrate()

            }
        }
    }
}