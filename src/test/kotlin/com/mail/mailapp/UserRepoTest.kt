package com.mail.mailapp


import com.mail.mailapp.model.User
import com.mail.mailapp.repo.UserRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Mono
import reactor.test.StepVerifier


@SpringBootTest
class UserRepoTest : AbstractIntegrationTest() {

    @Autowired
    lateinit var userRepository: UserRepository


    @Test
    fun saveUserTest() {
        userRepository.createByNickname("user1").block()
        val userMono: Mono<User> = userRepository.findByNickname("user1")
        StepVerifier
                .create(userMono)
                .assertNext { user: User ->
                    Assertions.assertEquals("user1", user.nickname)
                }
                .expectComplete()
                .verify()
    }

}
