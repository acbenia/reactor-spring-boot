package com.mail.mailapp


import com.mail.mailapp.model.Mail
import com.mail.mailapp.repo.MailRepository
import com.mail.mailapp.repo.UserRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class MailRepoTest : AbstractIntegrationTest() {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var mailRepository: MailRepository


    @Test
    fun createMail() {

        userRepository.createByNickname("user1").block()
        userRepository.createByNickname("user2").block()

        val mail = Mail()
        mail.sender = "user1"
        mail.receiver = "user2"
        mail.subject = "greeting"
        mail.body = "hello there"
        val resMail = mailRepository.save(mail).block()
        Assertions.assertThat(resMail?.iden).isNotNull()
    }

    @Test
    fun scanInbox() {

        userRepository.createByNickname("user1").block()
        userRepository.createByNickname("user2").block()

        val mail = Mail()
        mail.sender = "user1"
        mail.receiver = "user2"
        mail.subject = "greeting"
        mail.body = "hello there"
        val resMail = mailRepository.save(mail).block()

        mailRepository.getSentMails("user1").blockFirst()
    }
}
