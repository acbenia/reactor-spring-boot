### Mail APP

- This is a spring boot based app that allows for mail management.
- Its based on the reactor project which allows better resource management on the server side.

The Endpoint specified by the app are as follows

##### POST /users
to create a user
##### POST /mails/{user}
to send a mail from user {user}
##### GET /mails/{user}/inbox
to get mails inbox
##### GET /mails/{user}/sent
to get sent mails
